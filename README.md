# Comment créer un nouveau projet Angular

Pré-requis : Installer Node et npm

### Installer le CLI d'Angular :

CLI : Command Line Interface 
Permet de créer, gérer et déployer des applications Angular. Il nous faut l'installer globalement sur l'ordinateur pour pouvoir créer un projet Angular.

```
npm i -g @angular/cli
```

i pour "install" /
-g pour "global" (installé globalement sur l'ordinateur)

#### Afficher la version installée

```
ng v
```

Si le message d'erreur suivant apparaît :

```
.\script.ps1 : Impossible de charger le fichier C:\Users\WindowsFacile\Desktop\script.ps1, car l’exécution de scripts est désactivée sur ce système. Pour plus d’informations, consultez about_Execution_Policies à l’adresse https://go.microsoft.com/fwlink/?LinkID=135170.
Au caractère Ligne:1 : 1
+ .\script.ps1
+ ~~~~~~~~~~~~~~
+ CategoryInfo : Erreur de sécurité : (:) [], PSSecurityException
+ FullyQualifiedErrorId : UnauthorizedAccess
```

Ouvrez *Windows Powershell* en tant qu'administrateur et tapez cette commande :
```
set-executionpolicy unrestricted
```
Puis O pour "oui"

Reessayez la première commande, cela devrait avoir résolu le problème.

#### Créer une application Angular

La commande pour créer une nouvelle application avec Angular est 
```
ng new nom-de-votre-appli
```

Angular vous posera 2 questions, la première pour choisir le langage utilisé pour le style (css, scss, less) et la deuxième si vous souhaitez activer le rendu côté serveur (Server-Side Rendering - SSR) et la génération de site statique (Static Site Generation - SSG/Prerendering) pour votre projet Angular.

#### Lancer le serveur

Lancez le serveur avec cette commande à l'intérieur du dossier du projet
```
ng serve
```

Si vous souhaitez que la page s'ouvre directement dans le navigateur vous pouvez écrire 

```
ng serve --open
```

# Installer un projet Angular déjà existant

Pour installer toutes les dépendances d'un projet existant il faut aller dans le dossier du projet où se trouve "package.json" et lancer la commande :

```
npm install
```

Ensuite on lance le serveur de la même façon qu'un projet angular classique

```
ng serve
```